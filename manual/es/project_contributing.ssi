:B~ Contribuir al proyecto

1~contributing-to-project Contribuir al proyecto

Cuando se envía una contribución se debe identificar claramente al titular
de los derechos de autor e incluir la declaración de las licencias
aplicables. Se hace notar que para ser aceptada, una contribución debe ser
publicada bajo la misma licencia que el resto del documento, es decir, GPL
versión 3 o posterior.

Las contribuciones al proyecto, tales como traducciones y parches, son muy
bienvenidas. Cualquiera puede hacer una entrega en los repositorios, sin
embargo, a la hora de hacer grandes cambios, es conveniente enviarlos a la
lista de correo para debatirlos primero. Ver la sección {Contacto}#contact
para más información.

El ${project} utiliza Git como sistema de control de versiones y gestión de
código fuente. Como se explica en {Repositorios Git}#git-repositories hay
dos ramas principales de desarrollo: *{debian}* y *{debian-next}*. Todo el
mundo puede hacer entregas a las ramas debian-next de los repositorios
live-boot, live-build, live-config, live-images, live-manual y live-tools.

Sin embargo, existen ciertas restricciones. El servidor rechazará:

_* Entregas que no sean fast-forward

_* Entregas que hagan fusiones.

_* Añadir o borrar etiquetas o ramas.

A pesar de que todas las entregas pueden ser revisadas, pedimos usar el
sentido común y  hacer buenos commits con mensajes de commit adecuados.

_* Hay que escribir mensajes de entrega que consistan en una frase en ingles
con significado completo, comenzando con una letra mayúscula y acabando con
un punto final. Es habitual comenzar estas frases con la forma
'Fixing/Adding/Removing/Correcting/Translating/...'.

_* Escribir buenos mensajes de entrega. La primera frase debe ser un resumen
exacto de los contenidos del commit, que se incluirá en la lista de
cambios. Si se necesita hacer algunas aclaraciones, escribirlas debajo
dejando una línea en blanco después de la primera y luego otra línea en
blanco después de cada párrafo. Las líneas de los párrafos no deben superar
los 80 caracteres de longitud.

_* Hacer entregas de forma atómica, es decir, no mezclar cosas no
relacionadas en el mismo commit. Hacer un commit diferente para cada cambio
que se realice.

2~ Realizar cambios

Para hacer una entrega a los repositorios, se debe seguir el siguiente
procedimiento. Aquí se utiliza live-manual como ejemplo, por eso hay que
sustituirlo por el nombre del repositorio con el que se desea trabajar. Para
obtener información detallada sobre cómo editar live-manual ver {Contribuir
a este  documento}#how-to-contribute.

_* Obtener la clave pública de entrega:

code{

 $ mkdir -p ~/.ssh/keys
 $ wget http://debian-live.alioth.debian.org/other/keys/git@debian-live.alioth.debian.org -O ~/.ssh/keys/git@debian-live.alioth.debian.org
 $ wget http://debian-live.alioth.debian.org/other/keys/git@debian-live.alioth.debian.org.pub -O ~/.ssh/keys/git@debian-live.alioth.debian.org.pub
 $ chmod 0600 ~/.ssh/keys/git@debian-live.alioth.debian.org*

}code

_* Añadir la siguiente sección en el fichero de configuración de
openssh-client:

code{

 $ cat >> ~/.ssh/config << EOF
 Host debian-live.alioth.debian.org
     Hostname debian-live.alioth.debian.org
     User git
     IdentitiesOnly yes
     IdentityFile ~/.ssh/keys/git@debian-live.alioth.debian.org
 EOF

}code

_* Obtener un clon del manual mediante git utilizando ssh:

code{

 $ git clone ssh://git.debian.org/git/debian-live/live-manual.git
 $ cd live-manual && git checkout debian-next

}code

_* Acordarse de configurar el autor y el email en Git:

code{

  $ git config user.name "John Doe"
  $ git config user.email john@example.org

}code

*{Importante:}* Recordar que hay que enviar los cambios a la rama *{debian-next}*.

_* Efectuar los cambios. En este ejemplo, primero se escribiría una nueva
sección sobre cómo aplicar parches y luego se añadirían los ficheros y se
escribiría el mensaje de la siguiente manera:

code{

 $ git commit -a -m "Adding a section on applying patches."

}code

_* Para finalizar se realizará la entrega al servidor:

code{

 $ git push

}code

2~translation-of-manpages Traducción de las páginas de manual

También se puede contribuir al proyecto trabajando en la traducción de las
páginas de manual de los diferentes paquetes live-* que el proyecto
mantiene. El proceso es diferente, dependiendo de si se está comenzando una
traducción desde cero o si se continua trabajando en una traducción ya
comenzada:

_* Continuar con una traducción ya comenzada

Si se desea mantener la traducción de una lengua ya existente hay que
realizar los cambios en el fichero o ficheros presentes en
#{manpages/po/${LANGUAGE}/*.po}# y después ejecutar #{make rebuild}# desde
dentro del directorio #{manpages/}#. Esto actualizará las páginas de manual
de #{manpages/${LANGUAGE}/*}#

_* Empezar una nueva traducción desde cero

Para añadir una nueva traducción de cualquiera de las páginas de manual del
proyecto hay que seguir un proceso similar. Se puede resumir del modo
siguiente:

_2* Abrir el fichero o ficheros de #{manpages/pot/}# en el editor preferido,
como por ejemplo /{poedit}/, y guardarlo como fichero .po en
#{manpages/po/${LANGUAGE}/}#. (Se tendrá que crear el directorio del
#{${LANGUAGE}/}# correspondiente).

_2* Ejecutar #{make rebuild}# desde dentro del directorio #{manpages/}# para
crear los ficheros #{manpages/${LANGUAGE}/}# que contendrán las páginas de
manual.

Recordar que se tendrán que añadir todos los directorios y ficheros antes de
escribir el mensaje de presentación y hacer la entrega al servidor git.

